import React, { useEffect, useState } from "react";

import { FlatList, StatusBar, StyleSheet, Text, View } from "react-native";

import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Post from "../components/Post";

// import postsLocal from "../data/PostsLocal.js";
import { fetchPosts } from "../api/PostsApi";
import { SafeAreaView } from "react-native-safe-area-context";

const Home = ({ navigation }) => {
  const [posts, setPosts] = useState([]);

  const PressPostHandler = (postId) => {
    const aPost = posts.filter((p) => p.id === postId)[0];

    return navigation.navigate("PostDetail", {
      post: aPost,
    });
  };

  useEffect(() => {
    //console.log("useEffect", true);

    fetchPosts()
      .then((response) => {
        const res = response.slice(0, 10);
        setPosts(res);
      })
      .catch((error) => {
        alert(error);
      });
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Header title="Latest News" />
        <View style={styles.posts}>
          <FlatList
            data={posts}
            renderItem={(posts) => (
              <Post post={posts.item} onPressPost={PressPostHandler} />
            )}
            key={posts.id}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  wrapper: {
    flex: 1,
  },
  posts: {
    flex: 1,
  },
});

export default Home;
