import React from "react";

import { StyleSheet, Text, View, Button, SafeAreaView } from "react-native";

import Header from "../components/Header";
import Navbar from "../components/Navbar";

const PostDetail = ({ navigation, route }) => {
  const { post } = route.params;

  return (
    <View style={styles.container}>
      <Header title={post.title} />
      <Text>{post.body}</Text>
      <View style={styles.backButton}>
        <Button
          title="BACK"
          color="crimson"
          onPress={() => navigation.goBack()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  backButton: {
    flex: 1,
    marginTop: 20,
    height: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
    // alignSelf: "center",
  },
});

export default PostDetail;
