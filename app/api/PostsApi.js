import axios from "react-native-axios";
import { API_URL } from "./BaseApi";

export const fetchPosts = (userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const apiUrl = `${API_URL}/posts`;
      
      if (userId != null) 
        apiUrl += `?userId=${userId}`;

      const response = await axios.get(apiUrl);
      resolve(response.data);
    } catch (error) {
      reject(error);
    }
  });
};
