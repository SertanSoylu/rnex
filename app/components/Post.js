import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import { MaterialCommunityIcons } from "@expo/vector-icons";

const Post = ({ post, onPressPost }) => {
  const [liked, setLiked] = useState(false);
  const [forwarded, setForwarded] = useState(false);
  const [replied, setReplied] = useState(false);

  const handleReaction = (op) => {
    if (op === "heart") setLiked(!liked);
    else if (op === "forward") setForwarded(!forwarded);
    else if (op === "reply") setReplied(!replied);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          onPressPost(post.id);
        }}
      >
        <Text style={styles.title} numberOfLines={1}>
          {post.title}
        </Text>
        <Text style={styles.body} numberOfLines={2}>
          {post.body}
        </Text>
      </TouchableOpacity>
      <View style={styles.reactionPanel}>
        <TouchableOpacity onPress={() => handleReaction("heart")}>
          <MaterialCommunityIcons
            size={25}
            name={liked ? "heart" : "heart-outline"}
            color={liked ? "red" : "lightgray"}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleReaction("forward")}>
          <MaterialCommunityIcons
            name="forward"
            size={25}
            color={forwarded ? "blue" : "lightgray"}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleReaction("reply")}>
          <MaterialCommunityIcons
            name={replied ? "reply" : "reply-outline"}
            size={25}
            color={replied ? "green" : "lightgray"}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: 120,
    width: "100%",
    padding: 10,
    borderColor: "lightgray",
    borderRadius: 10,
    borderWidth: 1,
    marginBottom: 10,
  },
  title: {
    fontSize: 15,
    fontWeight: "bold",
  },
  body: {
    paddingLeft: 10,
  },
  reactionPanel: {
    flexDirection: "row",
    marginTop: 15,
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});

export default Post;
