import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";

import { MaterialIcons } from "@expo/vector-icons";

const Navbar = () => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.left}>
          <MaterialIcons name="menu" size={24} color="gray" />
        </View>
        <View style={styles.title}>
          <Text style={styles.titleText}>RNex</Text>
        </View>
        <View style={styles.right}>
          <MaterialIcons
            style={styles.rightButton}
            name="info-outline"
            size={24}
            color="gray"
          />
          <MaterialIcons
            style={styles.rightButton}
            name="message"
            size={24}
            color="gray"
          />
          <MaterialIcons
            style={styles.rightButton}
            name="person"
            size={24}
            color="gray"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "orange",
    height: 50,
    padding: 10,
  },
  wrapper: {
    width: "100%",
    height: "100%",
    flex: 1,
    flexDirection: "row",
  },
  left: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    height: "100%",
  },
  title: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
  },
  right: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    height: "100%",
  },
  rightButton: {
    marginLeft: 10,
  },
});

export default Navbar;
