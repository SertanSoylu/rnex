import React from "react";
import { StyleSheet, Text, View } from "react-native";

const Header = ( { title, subTitle } ) => {
  
  //console.log(title, subTitle);

  return (
    <View style={styles.infoHeader}>
      <Text style={styles.infoHeaderTitle}>
        {title == null ? "Latest News" : title }
      </Text>
      <Text style={styles.infoHeaderSubTitle}>
        {subTitle == null ? new Date().toLocaleString() : subTitle}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  infoHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
    borderColor: "transparent",
    borderBottomColor: "crimson",
    borderWidth: 2,
    marginBottom: 20,
  },
  infoHeaderTitle: {
    width: "85%",
    fontSize: 25,
  },
  infoHeaderSubTitle: {
    width: "15%",
    fontSize: 10,
  },
});

export default Header;
